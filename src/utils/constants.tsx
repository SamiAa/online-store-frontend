// constants.tsx

const constants = {
    costs: {
        PIZZA_NO_TOPPINGS: 6.90,
        TOPPING: 1.00,
        SMALL_DRINK: 2.50,
        LARGE_DRINK: 3.50
    }
}

export default constants;