// types.tsx

export type EnumLiteralsOf<T extends object> = T[keyof T];

export type Products = Array<Product>;

export type Product = {
    id: number,
    name: string,
    components: Components,
    cost: number,
    category: string
};

export type Components = {
    base: string,
    sauce: string,
    toppings: Array<string>
};

export type AllComponents = {
    bases: Array<string>,
    sauces: Array<string>,
    toppings: Array<string>
};

export type ArrayOfOrders = Array<Order>;

export type Order = {
    product: Product,
    amount: number,
    cost: number
};

export type FinalOrder = {
    user: String,
    deliveryAddress: String,
    products: ArrayOfOrders,
    cost: number,
    paymentMethod: string,
    flag: Flag,
    date: string
};

export type Address = {
    streetAddress: string,
    postNumber: string,
    city: string
};

export type Options = EnumLiteralsOf<typeof OptionsEnums>;

export const OptionsEnums = Object.freeze({
    NoSelection: "" as "",
    SelectMeat: "meat" as "meat",
    SelectVege: "vege" as "vege"
});

export type Flag = EnumLiteralsOf<typeof FlagEnums>;

export const FlagEnums = Object.freeze({
    Working: "Working" as "Working",
    Delivering: "Delivering" as "Delivering",
    Done: "Done" as "Done"
});

export type PaymentMethods = EnumLiteralsOf<typeof PaymentMethodOptions>;

export const PaymentMethodOptions = Object.freeze({
    Cash: "Cash" as "Cash",
    CreditCard: "Credit card" as "Credit card"
});

export type Users = Array<User>;

export type User = {
    username: string,
    password: string,
    streetAddress: string,
    postNumber: string,
    city: string
};