import React from 'react';

const LangContext = React.createContext("FI");

export default LangContext;