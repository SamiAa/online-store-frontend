import React from 'react';
import Logo from './components/Logo/Logo';
import Menu from './components/Menu/Menu';
import Editor from './components/Editor/Editor';
import Cart from './components/Cart/Cart';
import Login from './components/Login/Login';
import UserInformation from './components/UserInformation/UserInformation';
import SignUpForm from './components/SignUpForm/SignUpForm';
import ConfirmPage from './components/ConfirmPage/ConfirmPage';
import Orders from './components/Orders/Orders';
import LangContext from './utils/models/LangContext';
import { Router, Route, Switch, Link, Redirect } from 'react-router-dom';
import { Products, AllComponents, Product, ArrayOfOrders, OptionsEnums, Options, User, FinalOrder, FlagEnums, Address } from './utils/models/types';
import axios from 'axios';
import history from './utils/history';
import background from './utils/images/background2.svg';
import endLogo from './utils/images/end-logo.svg';
import cart from './utils/images/cart.svg';
import arrow from './utils/images/arrow.svg';
import strings from './App.json';
import './App.css';

// Data for testing purposes for pizzas
import testData from './data/test-data.json';

type Props = {};
type State = {
    user: User,
    cart: ArrayOfOrders,
    products: Products,
    components: AllComponents,
    drinks: Array<string>,
    chosenCategory: Options,
    lang: string
};

class App extends React.Component<Props, State> {

    state = {
        user: {} as User,
        cart: [] as ArrayOfOrders,
        products: [] as Products,
        components: {
            bases: [],
            sauces: [],
            toppings: []
        } as AllComponents,
        drinks: [] as Array<string>,
        chosenCategory: OptionsEnums.SelectMeat,
        lang: "FI"
    }

    /**
     * Get data of products on offer
     * @param lang: string
     */
    getProductsAndComponents = (lang: string) => {
        let data;

        if (lang === "FI") data = testData.FI;
        else data = testData.EN;

        return data;
    };

    /**
     * Add a product to cart or modified existing product
     * @param product: Product
     * @param amount: number
     * @param id: number
     */
    addToCart = (product: Product, amount: number, id: number) => {
        const cart = [...this.state.cart];
        const orderObject = {
            product: product,
            amount: amount,
            cost: amount * product.cost
        }
        const splitLocation = history.location.pathname.split("/");
        
        // When adding totally new product
        if (splitLocation[1] === "menu") cart.push(orderObject);
        // When modifying already added product
        else if (splitLocation[1] === "cart") cart.splice(id, 1, orderObject);

        this.setState({
            cart: cart
        });
    };

    /**
     * Delete a product from cart
     * @param id: number
     */
    deleteFromCart = (id: number) => {
        const cart = [...this.state.cart];

        cart.splice(id, 1);

        this.setState({
            cart: cart
        });
    };

    /**
     * Confirm the order by sending products in cart to the backend
     * @param address: Address
     * @param paymentMethod: string
     */
    confirmOrder = async (address: Address, paymentMethod: string) => {
        const date = new Date();
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const year = String(date.getFullYear());
        const hours = String(date.getHours()).padStart(2, '0');
        const minutes = String(date.getMinutes()).padStart(2, '0');
        const dateAndTime = `${day}.${month}.${year}; ${hours}:${minutes}`;
        let finalCost = 0;
        let returnValue: boolean = false;

        // Calculate total cost of the order
        for (let i = 0; i < this.state.cart.length; i++) {
            finalCost += this.state.cart[i].cost;
        }

        // Create an object from the cart's data and send it to the backend
        const finalOrder: FinalOrder = {
            user: this.state.user.username,
            deliveryAddress: `${address.streetAddress}, ${address.postNumber} ${address.city}`,
            products: this.state.cart,
            cost: finalCost,
            paymentMethod: paymentMethod,
            flag: FlagEnums.Working,
            date: dateAndTime
        };

        // Send the object the the backend
        try {
            const response = await axios.post(
                // 'http://localhost:4000/orders',
                `${process.env.BACKEND_ADDRESS}/orders`,
                finalOrder
            );

            if (response.status === 200) {
                // Empty the cart after successfull operation
                this.setState({ cart: [] as ArrayOfOrders });
                // Send true at the end of this function
                returnValue = true;
            }
        } catch (err) {
            console.log(err);
        }

        return returnValue;
    };

    /**
     * Request all orders of the logged user from the backend
     */
    getOrdersOfTheUser = async () => {
        let orders: Array<FinalOrder> = [] as Array<FinalOrder>

        try {
            const response = await axios.get(
                // `http://localhost:4000/users/${this.state.user.username}/orders`,
                `${process.env.BACKEND_ADDRESS}/users/${this.state.user.username}/orders`,
                {
                    headers: {
                        Authorization: `Bearer ${sessionStorage.getItem("token")}`
                    }
                }
            );

            if (response.status === 200) {
                orders = response.data.msg;
            }
        } catch (err) {
            console.log(err);
        }

        return orders;
    };

    /**
     * Choose what kind of pizzas should be viewed to the user: meat or vege
     * @param category: Options
     */
    chooseCategory = (category: Options) => {
        if (category !== this.state.chosenCategory)
            this.setState({ chosenCategory: category });
    };

    /**
     * Mark the "root" page that the user is currently visiting
     * by changing its color to black in the top bar
     * User button is shown only after logging in
     */
    markCurrentPage = () => {
        if (history.location.pathname.split("/")[1] === "menu") {
            document.querySelector(".homeLink")?.setAttribute("style", "filter: brightness(0%)");
            document.querySelector(".cartLink")?.setAttribute("style", "filter: brightness(100%)");
            document.querySelector(".loginLink")?.setAttribute("style", "filter: brightness(100%)");
            document.querySelector(".userButton")?.setAttribute("style", "filter: brightness(100%)");
        } else if (history.location.pathname.split("/")[1] === "cart") {
            document.querySelector(".cartLink")?.setAttribute("style", "filter: brightness(0%)");
            document.querySelector(".loginLink")?.setAttribute("style", "filter: brightness(100%)");
            document.querySelector(".userButton")?.setAttribute("style", "filter: brightness(100%)");
            document.querySelector(".homeLink")?.setAttribute("style", "filter: brightness(100%)");
        } else if (history.location.pathname.split("/")[1] === "login") {
            document.querySelector(".loginLink")?.setAttribute("style", "filter: brightness(0%)");
            document.querySelector(".userButton")?.setAttribute("style", "filter: brightness(0%)");
            document.querySelector(".homeLink")?.setAttribute("style", "filter: brightness(100%)");
            document.querySelector(".cartLink")?.setAttribute("style", "filter: brightness(100%)");
        }
    };

    /**
     * Log into the system by sending a request to the backend
     * @param username: string
     * @param password: string
     */
    login = async (username: string, password: string) => {
        let user: User | undefined;
        let returnValue: boolean = false;

        try {
            // const response = await axios.post('http://localhost:4000/login', {
            const response = await axios.post(
                `${process.env.BACKEND_ADDRESS}/login`,
                {
                    username: username,
                    password: password
                }
            );

            if (response.status === 200) {
                user = response.data.msg as User;

                // Set the user into the state if it is not done already
                // This will not happen when the user is logging in again in 
                // the user information page while trying to change username or password
                if (!this.state.user.username) this.setState({ user: user });

                // Save the token gotten from backend into the sessionStorage for later requests.
                sessionStorage.setItem("token", response.data.token);
                returnValue = true;
            }
        } catch (err) {
            console.log(err);
        }

        return returnValue;
    };

    /**
     * Log out by deleting the token and the user's information
     */
    logout = () => {
        this.setState({ user: {} as User });
        sessionStorage.removeItem("token");
        history.push("/menu");
    };

    /**
     * Create a new user by sending the new users information to the backend
     * @param user: User
     */
    signup = async (user: User) => {
        let returnValue: boolean = false;

        try {
            // const response = await axios.post('http://localhost:4000/users', user);
            const response = await axios.post(
                `${process.env.BACKEND_ADDRESS}/users`,
                user
            );

            if (response.status === 200) {
                returnValue = true;
            }
        } catch (err) {
            console.log(err);
        }

        return returnValue;
    };

    /**
     * Save the modified user information by sending it to the backend
     * Modifications can be done in the user information page
     * @param updatedUser: User
     */
    saveUserInfo = async (updatedUser: User) => {
        let returnValue: boolean = false;

        try {
            const response = await axios.put(
                // `http://localhost:4000/users/${this.state.user.username}`,
                `${process.env.BACKEND_ADDRESS}/users/${this.state.user.username}`,
                updatedUser,
                {
                    headers: {
                        Authorization: `Bearer ${sessionStorage.getItem("token")}`
                    }
                }
            );

            if (response.status === 200) {
                this.setState({ user: updatedUser });
                returnValue = true;
            }
        } catch (err) {
            console.log(err);
        }

        return returnValue;
    };

    /**
     * Change the language and save the selection in localStorage so the language
     * will be remembered when refreshing the page
     * @param lang: string
     */
    selectLanguage = (lang: string) => {
        const data = this.getProductsAndComponents(lang);
        this.setState({
            products: data.Pizzas,
            components: data.Components,
            drinks: data.Drinks,
            lang: lang
        });
        localStorage.setItem("lang", lang);

    };

    /**
     * This will be executed when the app page is loaded
     */
    componentDidMount() {
        // Execute this block if the user is not logged in
        if (!sessionStorage.getItem("token")) {
            let data;
            let chosenLang: string | null = localStorage.getItem("lang");
    
            // Check if language selection is saved in localStorage
            // and get products
            if (chosenLang !== null && chosenLang !== undefined) {
                data = this.getProductsAndComponents(chosenLang);
            } else {
                chosenLang = "FI";
                data = this.getProductsAndComponents(chosenLang);
            }

            this.setState({
                products: data.Pizzas,
                components: data.Components,
                drinks: data.Drinks,
                lang: chosenLang,
            });
        } else { // Execute this block if the user is logged in
            // Logged users information is requested from backend
            // by authenticating the user with the saved token
            new Promise(async (resolve, reject) => {
                let data;
                let chosenLang: string | null = localStorage.getItem("lang");

                // Check if language selection is saved in localStorage
                // and get products
                if (chosenLang !== null && chosenLang !== undefined) {
                    data = this.getProductsAndComponents(chosenLang);
                } else {
                    chosenLang = "FI";
                    data = this.getProductsAndComponents(chosenLang);
                }

                // The user information is requested by logging in with the saved token
                try {
                    const response = await axios.post(
                        // `http://localhost:4000/login`,
                        `${process.env.BACKEND_ADDRESS}/login`,
                        {},
                        {
                            headers: {
                                Authorization: `Bearer ${sessionStorage.getItem("token")}`
                            }
                        }
                    )

                    // User information is saved only if the request is successfull
                    if (response.status === 200) {
                        this.setState({
                            user: response.data.msg as User,
                            products: data.Pizzas,
                            components: data.Components,
                            drinks: data.Drinks,
                            lang: chosenLang
                        });
                        resolve('Logged in!');
                    } else { // Request was not succesfull if this block is executed
                        this.setState({
                            products: data.Pizzas,
                            components: data.Components,
                            drinks: data.Drinks,
                            lang: chosenLang
                        });
                        reject('Not logged in!');
                    }
                    
                } catch (err) {
                    reject('Not logged in!');
                }
            })
            .then(
                (value) => {
                    console.log(value);
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }

    /**
     * Create the app component
     */
    render() {
        let myStrings = strings.EN;

        // Set the correct strings depending on the chosen language
        if (this.state.lang === "FI") myStrings = strings.FI;

        return (
            <LangContext.Provider value={this.state.lang}>
                <Router history={history}>
                    <div className="App">
                        {/* Top bar */}
                        <div className="topBar">
                            {/* These links are on the left side of top bar */}
                            <Link to="/menu" className="homeLink">{myStrings.title}</Link>

                            {/* These links are on the right side of top bar */}
                            <div>
                                <div className="cartLink-wrapper">
                                    <Link to="/cart" className="cartLink">
                                        <img src={cart} className="cartImage" alt="cart" />
                                    </Link>
                                    {this.state.cart.length > 0 ?
                                        <div className="marker"></div> :
                                        null}
                                </div>

                                <div className="langSelect-wrapper">
                                    <button className="langButton">
                                        {myStrings.title6}
                                        <img src={arrow} className="arrowImage" alt="arrow" />
                                    </button>
                                    <div className="langOptions">
                                        <button
                                            className="langOptions-FI"
                                            onClick={() => this.selectLanguage("FI")}>
                                                {myStrings.title7}
                                        </button>
                                        <button
                                            className="langOptions-EN"
                                            onClick={() => this.selectLanguage("EN")}>
                                                {myStrings.title8}
                                        </button>
                                    </div>
                                </div>

                                {this.state.user.username === undefined ?
                                    <Link to="/login" className="loginLink">{myStrings.title2}</Link> :
                                    <div className="userWrapper">
                                        <button className="userButton">
                                            {this.state.user.username}
                                            <img src={arrow} className="arrowImage" alt="arrow" />
                                        </button>
                                        <div className="userOptions">
                                            <Link
                                                to={`/login/${this.state.user.username}/profile`}
                                                className="profileLink">
                                                    {myStrings.title3}
                                            </Link>
                                            <Link
                                                to={`/login/${this.state.user.username}/orders`}
                                                className="ordersLink">
                                                    {myStrings.title4}
                                            </Link>
                                            <button onClick={this.logout}>{myStrings.title5}</button>
                                        </div>
                                    </div>}

                            </div>
                        </div>

                        {/* Routes */}
                        <Switch>
                            <Route exact path="/login/:username/orders">
                                {this.state.user.username !== undefined ?
                                    <Orders
                                        getOrdersOfTheUser={this.getOrdersOfTheUser}
                                        markCurrentPage={this.markCurrentPage} /> :
                                        <Redirect to="/menu" />}
                            </Route>

                            <Route exact path="/login/:username/profile">
                                {this.state.user.username !== undefined ?
                                    <UserInformation
                                        user={this.state.user}
                                        login={this.login}
                                        saveUserInfo={this.saveUserInfo}
                                        markCurrentPage={this.markCurrentPage} /> :
                                        <Redirect to="/menu" />}
                            </Route>

                            <Route exact path="/login">
                                {this.state.user.username === undefined ?
                                    <Login markCurrentPage={this.markCurrentPage} login={this.login} /> :
                                    <Redirect to="/menu" />}
                            </Route>

                            <Route exact path="/signup">
                                {this.state.user.username === undefined ?
                                    <SignUpForm signup={this.signup} /> :
                                    <Redirect to="/menu" />}
                            </Route>

                            <Route exact path="/cart/confirm">
                                {this.state.cart.length > 0 ?
                                    <ConfirmPage
                                        user={this.state.user}
                                        cart={this.state.cart}
                                        confirmOrder={this.confirmOrder} /> :
                                    <Redirect to="/menu" />}
                            </Route>

                            <Route exact path="/cart/:id"
                                render={(props) => 
                                    this.state.cart.length > 0 ?
                                        <Editor cart={this.state.cart}
                                            products={this.state.products}
                                            components={this.state.components}
                                            addToCart={this.addToCart}
                                            {...props} /> :
                                        <Redirect to="/cart" />} />

                            <Route exact path="/cart">
                                <Cart cart={this.state.cart}
                                    deleteFromCart={this.deleteFromCart}
                                    markCurrentPage={this.markCurrentPage} />
                            </Route>

                            <Route exact path="/menu/:id"
                                render={(props) => 
                                    this.state.products.length > 0 ?
                                        <Editor cart={this.state.cart}
                                            products={this.state.products}
                                            components={this.state.components}
                                            addToCart={this.addToCart}
                                            {...props} /> :
                                        <Redirect to="/menu" />} />

                            <Route path="/menu">
                                <header className="App-header">
                                    <img src={background} className="background" alt="background" />
                                    <div className="background background-second"></div>
                                    <div className="foreground">
                                        <Logo />
                                        <p>{myStrings.message}</p>
                                        <h3>{myStrings.message2}</h3>
                                    </div>
                                </header>
                                <Menu products={this.state.products}
                                    chosenCategory={this.state.chosenCategory}
                                    chooseCategory={this.chooseCategory}
                                    markCurrentPage={this.markCurrentPage} />
                            </Route>

                            <Route path="/">
                                <Redirect to="/menu" />
                            </Route>

                        </Switch>

                        {/* Bottom bar */}
                        <div className="bottomBar">
                            <img src={endLogo} className="endLogo" alt="thumbs up" />
                        </div>

                    </div>
                </Router>
            </LangContext.Provider>
        );
    }
}

export default App;
