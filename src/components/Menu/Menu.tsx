import React from 'react';
import Pizza from '../Pizza/Pizza';
import LangContext from '../../utils/models/LangContext';
import tomato from '../../utils/images/tomato-color-border.svg';
import beef from '../../utils/images/beef-color-border.svg';
import { OptionsEnums, Products, Product, Options } from '../../utils/models/types';
import strings from './Menu.json';
import './Menu.css';

type Props = {
    products: Products,
    chosenCategory: Options,
    chooseCategory: Function,
    markCurrentPage: Function
};
type State = {};

class Menu extends React.Component<Props, State> {

    // Get th context for setting correct strings depending on the chosen language
    static contextType = LangContext;

    /**
     * Select a category: meat or vege pizzas
     * @param selection: Options
     */
    select = (selection: Options) => {
        const categoryButtons = document.querySelectorAll(".Menu-nav button");

        // Set both category buttons opacity low to indicate inactiveness
        for (let i = 0; i < categoryButtons.length; i++) {
            categoryButtons[i].setAttribute("style", "filter: opacity(0.12)");
        }

        // If user selected meat pizza category, set that button's opacity to normal
        // and slide the meat pizzas into the screen
        if (selection === OptionsEnums.SelectMeat) {
            categoryButtons[0].setAttribute("style", "filter: opacity(1)");
            document.querySelector(".Menu-slider")?.setAttribute("style", "transform: translateX(-33.33%)");
        } else if (selection === OptionsEnums.SelectVege) { // If the user selected vege pizza category
            // set that button's opacity to normal
            categoryButtons[1].setAttribute("style", "filter: opacity(1)");
            // and slide the vege pizzas into the screen
            document.querySelector(".Menu-slider")?.setAttribute("style", "transform: translateX(-66.66%)");
        } else { // If neither option is selected
            document.querySelector(".Menu-slider")?.setAttribute("style", "transform: translateX(0)");
        }

        this.props.chooseCategory(selection);
    };

    // Execute this when Menu component is loaded
    componentDidMount() {
        this.select(this.props.chosenCategory);
        this.props.markCurrentPage();
    };

    // Create the Menu component
    render() {
        const meatPizzas: Products = [];
        const vegePizzas: Products = [];
        const nonCategorizedPizzas: Products = [];
        let product: Product;
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        // Loop through all products and
        // push meat pizzas into one array
        // and vege pizzas into the other array
        // Non categorized pizzas go into their own
        // array also
        for (let i = 0; i < this.props.products.length; i++) {
            product = this.props.products[i];
            if (product.category === OptionsEnums.SelectMeat) {
                meatPizzas.push(product);
            } else if (product.category === OptionsEnums.SelectVege) {
                vegePizzas.push(product);
            } else {
                nonCategorizedPizzas.push(product);
            }
        }

        return (
            <div className="Menu">
                {/* Title */}
                <h1>{myStrings.title}</h1>

                {/* Buttons to choose category with */}
                <div className="Menu-nav">
                    <button
                        data-info={myStrings.title2}
                        onClick={() => this.select(OptionsEnums.SelectMeat)}>
                        <img src={beef} alt="meat" />
                    </button>
                    <button
                        data-info={myStrings.title3}
                        onClick={() => this.select(OptionsEnums.SelectVege)}>
                        <img src={tomato} alt="vege" />
                    </button>
                </div>

                {/* List of pizzas */}
                <div className="Menu-list">
                    {/* Non-categorizes pizzas to the non-sliding part on top */}
                    {nonCategorizedPizzas.map((product, i) => 
                        <Pizza product={product} key={i} />)}
                    
                    {/* Sliding part of the list */}
                    <div className="Menu-slider">
                        {/* Visible when category is not chosen */}
                        <div className="Menu-slider-start"></div>
                        {/* Visible when meat category is chosen */}
                        <div className="Menu-slider-meat">
                            {meatPizzas.map((product, i) =>
                                <Pizza product={product} key={i} />
                            )}
                        </div>
                        {/* Visible when vege category is chosen */}
                        <div className="Menu-slider-vege">
                            {vegePizzas.map((product, i) =>
                                <Pizza product={product} key={i} />
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu;
