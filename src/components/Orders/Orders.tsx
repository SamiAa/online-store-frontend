import React from 'react';
import history from '../../utils/history';
import LangContext from '../../utils/models/LangContext';
import { FinalOrder } from '../../utils/models/types';
import LoadingImage from '../LoadingImage/LoadingImage';
import strings from './Orders.json';
import './Orders.css';

type Props = {
    getOrdersOfTheUser: Function,
    markCurrentPage: Function
};
type State = {
    orders: Array<FinalOrder>
};

class Orders extends React.Component<Props, State> {

    // Get context to set correct strings depending on the chosen language
    static contextType = LangContext;

    state = {
        orders: [] as Array<FinalOrder>
    };

    /**
     * Executed when this component is loaded
     */
    componentDidMount() {
        let orders: Array<FinalOrder> = [] as Array<FinalOrder>;

        // wait for the logged in users orders to be requested from the backend
        new Promise(async (resolve, reject) => {
            orders = await this.props.getOrdersOfTheUser() as Array<FinalOrder>

            if (orders.length > 0) {
                resolve('Orders were received successfully!');
            } else {
                reject('Orders could not be found!');
            }
        })
        .then(
            (value) => {
                this.setState({
                    orders: orders
                });
                // Hide the loading image
                document.querySelector(".Orders .LoadingImage")?.setAttribute("style", "background-color: transparent");
                document.querySelector(".Orders .LoadingImage img")?.setAttribute("style", "display: none");
            },
            (error) => {
                console.log(error);
                // Hide the loading image
                document.querySelector(".Orders .LoadingImage")?.setAttribute("style", "background-color: transparent");
                document.querySelector(".Orders .LoadingImage img")?.setAttribute("style", "display: none");
            }
        );

        this.props.markCurrentPage();
    }

    /**
     * Create the Orders component
     */
    render() {
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        return (
            <div className="Orders">
                {/* Title */}
                <h1>{myStrings.title}</h1>

                {/* Content */}
                <div className="Orders-content">
                    {this.state.orders.length > 0 ?
                        this.state.orders.map((order, i) =>
                            <div className="order" key={i}>
                                {/* Orders header */}
                                <div className="order-header">
                                    <div className="order-date">{order.date.split("; ")[0]}</div>
                                    <div className="order-date-2">{order.date.split("; ")[1]}</div>
                                    <div className="cost">{order.cost.toFixed(2)}€</div>
                                </div>

                                {/* Orders products */}
                                <div className="order-products">
                                    {order.products.map((product, j) =>
                                        <div className="product" key={j}>
                                            <div className="marker"
                                                style={{ backgroundColor: product.product.category === "meat" ?
                                                    "rgb(170, 0, 0)" :
                                                    product.product.category === "vege" ?
                                                    "rgb(51, 120, 0)" :
                                                    "black" }}></div>

                                            {/* Products header */}
                                            <div className="product-header">
                                                <div className="product-title">{product.product.name} x {product.amount}</div>
                                                <div className="product-cost">{product.cost.toFixed(2)}€</div>
                                            </div>

                                            {/* Products components */}
                                            <div className="product-components">
                                                <div className="product-components-base">
                                                    <span className="title">{myStrings.title2}</span>
                                                    <span>{product.product.components.base}</span>
                                                </div>

                                                <div className="product-components-sauce">
                                                    <span className="title">{myStrings.title3}</span>
                                                    <span>{product.product.components.sauce}</span>
                                                </div>
                                                
                                                <div className="product-components-toppings">
                                                    <span className="title">{myStrings.title4}</span>
                                                    {product.product.components.toppings.map((topping, k) =>
                                                        <span key={k}>{topping}</span>)}
                                                </div>
                                            </div>
                                        </div>)}
                                        
                                </div>
                            </div>) :
                            <div className="message">{myStrings.message}</div>}

                    <LoadingImage />
                </div>

                {/* Navigation buttons */}
                <div className="Orders-buttons">
                    <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button}</button>
                </div>
            </div>
        );
    }
}

export default Orders;