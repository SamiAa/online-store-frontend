import React from 'react';
import LangContext from '../../utils/models/LangContext';
import { ArrayOfOrders } from '../../utils/models/types';
import history from '../../utils/history';
import strings from './Cart.json';
import './Cart.css';

type Props = {
    cart: ArrayOfOrders,
    deleteFromCart: Function,
    markCurrentPage: Function
};
type State = {};

class Cart extends React.Component<Props, State> {

    // Get the context for setting correct strings
    static contextType = LangContext;

    /**
     * This is executed when the Cart page is loaded
     */
    componentDidMount() {
        this.props.markCurrentPage();
    };

    /**
     * Create the Cart
     */
    render() {
        let color1 = "rgb(170, 0, 0)";
        let color2 = "rgb(51, 120, 0)";
        let color3 = "black";
        let totalCost = 0;
        let myStrings = strings.EN;

        if (this.context === "FI") myStrings = strings.FI;

        // Calculate the total cost of the products in the cart
        for (let i = 0; i < this.props.cart.length; i++) {
            totalCost += this.props.cart[i].cost;
        }

        return (
            <div className="Cart">
                <div className="Cart-content">
                    {/* Header */}
                    <div className="Cart-content-header">
                        <h1>{myStrings.title}</h1>
                        <div>{totalCost.toFixed(2)}€</div>
                    </div>

                    {/* Content */}
                    <div>
                        {this.props.cart.length > 0 ? this.props.cart.map((order, i) => 
                            <div className="order" key={i}>
                                <div className="marker"
                                    style={order.product.category === "meat" ?
                                        { backgroundColor: color1 } :
                                        order.product.category === "vege" ?
                                            { backgroundColor: color2 } :
                                            { backgroundColor: color3 }}>
                                </div>

                                <div className="order-header">
                                    <h3>#{order.product.id + 1} {order.product.name}</h3>
                                    <div className="title">{order.cost.toFixed(2)}€</div>
                                </div>

                                <div className="order-content">
                                    <span className="title">{myStrings.title2}:</span>
                                    <span className="content">{order.product.components.base}</span>
                                    <br/>
                                    <span className="title">{myStrings.title3}:</span>
                                    <span className="content">{order.product.components.sauce}</span>
                                    <br/>
                                    <span className="title">{myStrings.title4}:</span>
                                    {order.product.components.toppings.map((topping, i) => 
                                        <span className="content" key={i}>{topping}</span>)}
                                    <br/>
                                    <span className="title">{myStrings.title5}:</span>
                                    <span className="content">{order.amount}</span>
                                    <br/>
                                </div>

                                <div className="order-buttons">
                                    <button type="button" onClick={() => this.props.deleteFromCart(i)}>{myStrings.button4}</button>
                                    <button type="button" onClick={() => history.push(`/cart/${i}`)}>{myStrings.button3}</button>
                                </div>   
                            </div>) :
                            <div className="message">{myStrings.message}</div>}
                    </div>
                </div>

                {/* Buttons in the bottom of the page */}
                <div className="Cart-buttons">
                    <button className="okButton" type="button" 
                        onClick={() => this.props.cart.length > 0 ? history.push("/cart/confirm") : {}}
                        style={ this.props.cart.length > 0 ? { filter: "brightness(115%)" } : { filter: "brightness(85%)" }}>
                            {myStrings.button}
                    </button>
                    <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button2}</button>
                </div>
            </div>
        );
    };
}

export default Cart;