import React from 'react';
import LoadingImage from '../LoadingImage/LoadingImage';
import LangContext from '../../utils/models/LangContext';
import { Link } from 'react-router-dom';
import history from '../../utils/history';
import strings from './Login.json';
import './Login.css';

type Props = {
    markCurrentPage: Function,
    login: Function
};
type State = {
    username: string,
    password: string
};

class Login extends React.Component<Props, State> {

    // Get the context for setting correct
    // strings depending on the chosen language
    static contextType = LangContext;

    state = {
        username: "",
        password: ""
    };

    /**
     * Change the correct state depending on the modified input field
     * @param event: React.ChangeEvent<HTMLInputElement>
     */
    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value } as Pick<State, keyof State>);
    };

    /**
     * Prepare the data for sending the given creadentials
     * to the backend and loggin in to the system
     * @param event: React.FormEvent
     */
    submitForm = (event: React.FormEvent) => {
        const username = this.state.username;
        const password = this.state.password;
        const usernameInput = document.querySelector(".Login .usernameInput");
        const passwordInput = document.querySelector(".Login .passwordInput");

        // If the user is not modifying creantial information,
        // this block is executed
        if (username !== "" && password !== "") {
                // Set  the input fields shadow back to normal in case it's not
                usernameInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
                passwordInput!.setAttribute("style", "box-shadow: 0 0 2px grey");

                let isCorrect: boolean;
                
                // Show the loading image so the user knows something is happening
                document.querySelector(".Login .LoadingImage")?.setAttribute("style", "background-color: rgb(230, 230, 230)");
                document.querySelector(".Login .LoadingImage img")?.setAttribute("style", "display: inline");
                document.querySelector(".Login .LoadingImage")?.setAttribute("style", "width: 105%");

                // Wait for the end of login process
                new Promise(async (resolve, reject) => {
                    isCorrect = await this.props.login(username, password);
                    
                    if (isCorrect)
                        resolve('Logged in successfully!');
                    else
                        reject('Wrong username or password!')
                })
                .then(
                    (value) => {
                        // Hide the message in case it's visible
                        document.querySelector(".Login .message")?.setAttribute("style", "visibility: hidden");
                        history.goBack();
                    },
                    (error) => {
                        console.log(error);
                        // Hide the loading image and show the message on error
                        document.querySelector(".Login .message")?.setAttribute("style", "visibility: visible");
                        document.querySelector(".Login .LoadingImage")?.setAttribute("style", "background-color: transparent");
                        document.querySelector(".Login .LoadingImage img")?.setAttribute("style", "display: none");
                        document.querySelector(".Login .LoadingImage")?.setAttribute("style", "width: 0%");
                    }
                );
        } else if (username === "" && password !== "") { // If username field is not filled
            // Set the username field's shadow red
            usernameInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            passwordInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
        } else if (password === "" && username !== "") { // If password field is not filled
            // Set the password field's shadow red
            usernameInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
            passwordInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
        } else { // If both username and password field are not filled
            // Set the shadow of both input fields red
            usernameInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            passwordInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
        }

        event.preventDefault();
    };

    // Execute when this component is loaded
    componentDidMount() {
        // Hide the loading image
        document.querySelector(".Login .LoadingImage")?.setAttribute("style", "background-color: transparent");
        document.querySelector(".Login .LoadingImage img")?.setAttribute("style", "display: none");
        document.querySelector(".Login .LoadingImage")?.setAttribute("style", "width: 0%");
        // Mark the current pages link black in the top bar
        this.props.markCurrentPage();
    };

    // Create the Login component
    render() {
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        return (
            <div className="Login">
                {/* Title */}
                <h1>{myStrings.title}</h1>

                {/* Content */}
                <div className="Login-content">
                    <form onSubmit={this.submitForm}>
                        {/* Username fields */}
                        <label>
                            {myStrings.title2}
                            <input
                                className="usernameInput"
                                name="username"
                                type="text"
                                value={this.state.username}
                                onChange={this.handleInputChange} />
                        </label>

                        {/* Password field */}
                        <label>
                            {myStrings.title3}
                            <input
                                className="passwordInput"
                                name="password"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleInputChange} />
                        </label>

                        {/* Messages */}
                        <div className="message">{myStrings.message}</div>
                        <div className="message2">
                            <Link to="/signup">{myStrings.message2}</Link>
                        </div>

                        {/* Navigation buttons */}
                        <button className="okButton" type="submit">{myStrings.button}</button>
                        <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button2}</button>
                    </form>
                    
                    <LoadingImage />
                </div>
            </div>
        );
    };
}

export default Login;