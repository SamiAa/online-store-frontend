import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import LangContext from '../../utils/models/LangContext';
import { Products, Product, AllComponents, EnumLiteralsOf, Components, ArrayOfOrders } from '../../utils/models/types';
import history from '../../utils/history';
import constants from '../../utils/constants';
import strings from './Editor.json';
import './Editor.css';

type Props = {
    cart: ArrayOfOrders,
    products: Products,
    components: AllComponents,
    addToCart: Function
};
type State = {
    product: Product,
    amount: number
};
type Params = {
    id: string
}
type Options = EnumLiteralsOf<typeof OptionsEnums>;
const OptionsEnums = Object.freeze({
    Base: "base" as "base",
    Sauce: "sauce" as "sauce",
    Toppings: "toppings" as "toppings"
});

class Editor extends React.Component<Props & RouteComponentProps<Params>, State> {

    // Get the language context for setting correct strings
    static contextType = LangContext;

    /**
     * Get product to edit
     * When editing a product on menu, the request parameter points
     * to the index of a product in the list of all products
     * When editing a product on cart, the request paremeter points
     * to the index of a product in cart
     */
    getProduct = () => {
        let currentProduct: Product;
        let product!: Product;
        const splitLocation = this.props.history.location.pathname.split("/");

        // If the location is in /menu
        if (splitLocation[1] === "menu") {
            for (let i = 0; i < this.props.products.length; i++) {
                currentProduct = this.props.products[i];
                if (currentProduct.id === parseInt(this.props.match.params.id) - 1) {
                    product = {
                        id: currentProduct.id,
                        name: currentProduct.name,
                        components: {
                            base: currentProduct.components.base,
                            sauce: currentProduct.components.sauce,
                            toppings: [ ...currentProduct.components.toppings ]
                        },
                        cost: currentProduct.cost,
                        category: currentProduct.category
                    };
                    break;
                }
            }
        } else if (splitLocation[1] === "cart") { // If the location is in /cart
            const order = this.props.cart[parseInt(this.props.match.params.id)];

            product = {
                id: order.product.id,
                name: order.product.name,
                components: {
                    base: order.product.components.base,
                    sauce: order.product.components.sauce,
                    toppings: [ ...order.product.components.toppings ]
                },
                cost: order.product.cost,
                category: order.product.category
            };
        }

        return product;
    };

    /**
     * Find a product from the list of all products which
     * matches the chosen pizza components
     * @param components: Components
     */
    findMatchingProduct = (components: Components) => {
        let currentProduct: Product;
        let matchingProduct: Product | undefined;
        let isSame: boolean;

        // Loop through all products
        for (let i = 0; i < this.props.products.length; i++) {
            isSame = false;
            currentProduct = this.props.products[i];
            // If a pizza in the list of all pizzas has the same base
            // that the user chose, we should check the next if statement
            if (components.base === currentProduct.components.base) {
                // If a pizza in the list of all pizzas has the same sauce
                // that the user chose, we should check the next if statement
                if (components.sauce === currentProduct.components.sauce) {
                    let length1 = components.toppings.length;
                    let length2 = currentProduct.components.toppings.length;
                    // If a pizza in the list of all pizzas has as many toppings
                    // than the user has chosen, we should go further
                    if (length1 === length2) {
                        let wasFound: boolean;
                        // Loop through toppings of the possibly matching pizza
                        for (let j = 0; j < length2; j++) {
                            wasFound = false;
                            // Loop through toppings of the users pizza
                            for (let k = 0; k < length1; k++) {
                                // Does the users pizza have the topping of
                                // the possibly matching pizza 
                                if (currentProduct.components.toppings[j] === components.toppings[k]) {
                                    wasFound = true;
                                    break;
                                }
                            }
                            // If the users chosen pizza did not have the topping
                            // in the possibly matching pizza, we know it's not a match
                            // so we can stop comparing
                            if (!wasFound)
                                break;
                            // If the users chosen pizza had the topping in
                            // the possibly chosen pizza, we can continue comparing
                            else if (j === length2 - 1)
                                isSame = true;
                        }
                    }
                }
            }

            // After checking all products, did we find a match?
            if (isSame === true) {
                matchingProduct = currentProduct;
                break;
            }
        }

        return matchingProduct;
    };

    /**
     * Choose a component for the users pizza
     * @param component: Options
     */
    toggleChoose = (component: Options) => {
        let myStrings = strings.EN;

        // Set the correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        let newProduct: Product;
        let matchingProduct: Product | undefined;

        // If the user chose a base component option
        if (component === OptionsEnums.Base) {
            newProduct = {
                id: 0,
                name: myStrings.title5,
                components: {
                    base: document.activeElement!.innerHTML,
                    sauce: this.state.product.components.sauce,
                    toppings: this.state.product.components.toppings
                },
                cost: this.state.product.cost,
                category: ""
            }

            // Check if the pizza is defined in the list of all pizzas
            matchingProduct = this.findMatchingProduct(newProduct.components);

            const splitLocation = this.props.history.location.pathname.split("/");
            // If we did find a matching pizza from the list of all pizzas
            if (matchingProduct !== undefined) {
                newProduct.id = matchingProduct.id;
                newProduct.name = matchingProduct.name;
                newProduct.category = matchingProduct.category;
                // If the user is not editing a product in the cart
                // we should edit the path to match with number of the matching pizza
                if (splitLocation[1] !== "cart" && splitLocation[2] !== `${matchingProduct.id + 1}`)
                    this.props.history.replace({ pathname: `/${splitLocation[1]}/${matchingProduct.id + 1}` });
            } else { // If we did not find a mathing pizza
                // If the user is not editing a product in the cart
                // we should edit the path to indicate it is a custom pizza
                if (splitLocation[1] !== "cart" && splitLocation[2] !== `${newProduct.id + 1}`) 
                    this.props.history.replace({ pathname: `/${splitLocation[1]}/${newProduct.id + 1}` });
            }

            this.setState({ product: newProduct });
        } else if (component === OptionsEnums.Sauce) { // If the user chose a sauce component option
            newProduct = {
                id: 0,
                name: myStrings.title5,
                components: {
                    base: this.state.product.components.base,
                    sauce: document.activeElement!.innerHTML,
                    toppings: this.state.product.components.toppings
                },
                cost: this.state.product.cost,
                category: ""
            };

            // Check if the pizza is defined in the list of all pizzas
            matchingProduct = this.findMatchingProduct(newProduct.components);

            const splitLocation = this.props.history.location.pathname.split("/");
            // If we did find a matching pizza from the list of all pizzas
            if (matchingProduct !== undefined) {
                newProduct.id = matchingProduct.id;
                newProduct.name = matchingProduct.name;
                newProduct.category = matchingProduct.category;
                // If the user is not editing a product in the cart
                // we should edit the path to match with number of the matching pizza
                if (splitLocation[1] !== "cart" && splitLocation[2] !== `${matchingProduct.id + 1}`)
                    this.props.history.replace({ pathname: `/${splitLocation[1]}/${matchingProduct.id + 1}` });
            } else { // I we did not find a matching pizza
                // If the user is not editing a product in the cart
                // we should edit the path to indicate it is a custom pizza
                if (splitLocation[1] !== "cart" && splitLocation[2] !== `${newProduct.id + 1}`) 
                    this.props.history.replace({ pathname: `/${splitLocation[1]}/${newProduct.id + 1}` });
            }

            this.setState({ product: newProduct });
        } else { // If the user chose a topping option
            const toppings = this.state.product.components.toppings;
            const selected = document.activeElement!.innerHTML;

            // Add the topping into the list of chosen toppings
            // if it not yet added there, otherwise we should remove
            // it from the afore mentioned list
            if (!toppings.includes(selected)) {
                toppings.push(document.activeElement!.innerHTML)
            } else {
                for (let i = 0; i < toppings.length; i++) {
                    if (toppings[i] === selected) {
                        toppings.splice(i, 1);
                        break;
                    }
                }
            }
            
            // Create a new object for the pizza with the user's
            // chosen components
            newProduct = {
                id: 0,
                name: myStrings.title5,
                components: {
                    base: this.state.product.components.base,
                    sauce: this.state.product.components.sauce,
                    toppings: toppings
                },
                cost: constants.costs.PIZZA_NO_TOPPINGS + 
                    toppings.length * constants.costs.TOPPING,
                category: ""
            }

            // Check if the pizza is defined in the list of all pizzas
            matchingProduct = this.findMatchingProduct(newProduct.components);

            const splitLocation = this.props.history.location.pathname.split("/");
            // If we did find a matching pizza from the list of all pizzas
            if (matchingProduct !== undefined) {
                newProduct.id = matchingProduct.id;
                newProduct.name = matchingProduct.name;
                newProduct.category = matchingProduct.category;
                // If the user is not editing a product in the cart
                // we should edit the path to match with number of the matching pizza
                if (splitLocation[1] !== "cart" && splitLocation[2] !== `${matchingProduct.id + 1}`)
                    this.props.history.replace({ pathname: `/${splitLocation[1]}/${matchingProduct.id + 1}` });
            } else { // If we did not find a matching pizza
                // If the user is not editing a product in the cart
                // we should edit the path to indicate it is a custom pizza
                if (splitLocation[1] !== "cart" && splitLocation[2] !== `${newProduct.id + 1}`) 
                    this.props.history.replace({ pathname: `/${splitLocation[1]}/${newProduct.id + 1}` });
            }

            // Save the pizza into the state
            this.setState({ product: newProduct });
        }
    };

    // Execute this when the Editor component is loaded
    componentDidMount() {
        const order = this.props.cart[parseInt(this.props.match.params.id)];
        const splitLocation = this.props.history.location.pathname.split("/");
        let amount = 1;

        // If the user is editing a pizza in the cart,
        // we should get the amount variable's value from
        // the order in the cart.
        if (splitLocation[1] === "cart")
            amount = order.amount;

        this.setState({
            product: this.getProduct(),
            amount: amount
        });
    }

    // Create the Editor
    render() {
        let product: Product;
        let amount: number;
        const bgColorChosen = "rgb(230, 209, 162)";
        const colorChosen = "black";
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        // If the state is not declared yet,
        // we should get the product to edit first
        // This can happen on the first render
        if (this.state === null) {
            product = this.getProduct();
            amount = 1;
        } else {
            product = this.state.product;
            amount = this.state.amount;
            // Do not let the amount's value go below 1
            if (amount < 1)
                amount = 1;
        }

        return (
            <div className="Editor">
                <div className="Editor-content">
                    {/* Title */}
                    <h1>#{product.id + 1} {product.name}</h1>

                    {/* Components */}
                    <div className="Editor-components">
                        {/* Bases */}
                        <div className="title">{myStrings.title}:</div>
                        <div className="options">
                            {this.props.components.bases.map((base, i) => 
                                product.components.base === base ?
                                    <button style={{
                                            backgroundColor: bgColorChosen,
                                            color: colorChosen
                                        }} 
                                        onClick={() => this.toggleChoose(OptionsEnums.Base)}
                                        key={i}>{base}</button> : 
                                    <button onClick={() => this.toggleChoose(OptionsEnums.Base)}
                                        key={i}>{base}</button>)}
                        </div>

                        {/* Sauces */}
                        <div className="title">{myStrings.title2}:</div>
                        <div className="options">
                            {this.props.components.sauces.map((sauce, i) => 
                                product.components.sauce === sauce ?
                                    <button style={{
                                            backgroundColor: bgColorChosen,
                                            color: colorChosen
                                        }}
                                        onClick={() => this.toggleChoose(OptionsEnums.Sauce)}
                                        key={i}>{sauce}</button> : 
                                    <button onClick={() => this.toggleChoose(OptionsEnums.Sauce)}
                                        key={i}>{sauce}</button>)}
                        </div>

                        {/* Toppings */}
                        <div className="title">{myStrings.title3}:</div>
                        <div className="options">
                            {this.props.components.toppings.map((topping, i) => 
                                product.components.toppings.includes(topping) ?
                                    <button style={{
                                            backgroundColor: bgColorChosen,
                                            color: colorChosen
                                        }}
                                        onClick={() => this.toggleChoose(OptionsEnums.Toppings)}
                                        key={i}>{topping}</button> : 
                                    <button onClick={() => this.toggleChoose(OptionsEnums.Toppings)}
                                        key={i}>{topping}</button>)}
                        </div>
                    </div>

                    {/* Amount and cost */}
                    <div className="Editor-finalInfo">
                        {/* On the left side of the block */}
                        <div className="Editor-finalInfo-first">
                            <div className="title">{myStrings.title4}:</div>
                            <button onClick={() => this.setState({ amount: amount + 1 })}>+</button>
                            <span>{amount}</span>
                            <button onClick={() => amount > 1 ? this.setState({ amount: amount - 1 }) : {}}>-</button>
                        </div>
                        {/* On the right side of the block */}
                        <div className="Editor-finalInfo-second title">{(amount * product.cost).toFixed(2)}€</div>
                    </div>
                </div>

                {/* Navigation buttons */}
                <div className="Editor-buttons">
                    <button className="okButton" type="button"onClick={() => {
                            this.props.addToCart(this.state.product, this.state.amount, this.props.match.params.id); history.goBack();
                        }}>{myStrings.button}
                    </button>
                    <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button2}</button>
                </div>
            </div>
        );
    }
}

export default Editor;