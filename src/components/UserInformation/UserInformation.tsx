import React from 'react';
import LangContext from '../../utils/models/LangContext';
import history from '../../utils/history';
import { User } from '../../utils/models/types';
import LoadingImage from '../LoadingImage/LoadingImage';
import strings from './UserInformation.json';
import './UserInformation.css';

type Props = {
    user: User,
    login: Function,
    saveUserInfo: Function,
    markCurrentPage: Function
};
type State = {
    isAuthenticated: boolean,
    oldPassword: string,
    username: string,
    password: string,
    streetAddress: string,
    postNumber: string,
    city: string
};

class UserInformation extends React.Component<Props, State> {

    // Get the context for setting the strings depending on the chosen language
    static contextType = LangContext;

    state = {
        isAuthenticated: false,
        oldPassword: "" as string,
        username: this.props.user.username,
        password: "thisisnothterealpassword",
        streetAddress: this.props.user.streetAddress,
        postNumber: this.props.user.postNumber,
        city: this.props.user.city
    }

    /**
     * Change the correct state depending of the modified input field
     * @param event:: React.ChangeEvent<HTMLInputElement>
     */
    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        let hasRight: boolean = false;

        // Neither password nor username was modified
        if (name !== "password" && name !== "username") {
            hasRight = true;
        } else { // Otherwise check if they have a right to change those
            if (this.state.isAuthenticated)
                hasRight = true;
        }

        // Set the correct state if they have a right to modify it
        if (hasRight) {
            if (name === "oldPassword") this.setState({ oldPassword: value });
            if (name === "username") this.setState({ username: value });
            if (name === "password") this.setState({ password: value });
            if (name === "streetAddress") this.setState({ streetAddress: value });
            if (name === "postNumber") this.setState({ postNumber: value });
            if (name === "city") this.setState({ city: value });
        }
    };

    /**
     * Prepare data for sending it to the backend
     * for user informations update process
     * @param event: React.FormEvent
     */
    submitForm = (event: React.FormEvent) => {
        const username = this.state.username;
        const password = this.state.password;
        const streetAddress = this.state.streetAddress;
        const postNumber = this.state.postNumber;
        const city = this.state.city;

        // Set the color of the shadow of the input field red if the field is empty

        if (username === "") {
            document.querySelector(".UserInformation .usernameInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px rgb(209, 0, 0)"
            );
        } else {
            document.querySelector(".UserInformation .usernameInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px grey"
            );
        }

        if (password === "") {
            document.querySelector(".UserInformation .passwordInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px rgb(209, 0, 0)"
            );
        } else {
            document.querySelector(".UserInformation .passwordInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px grey"
            );
        }

        if (streetAddress === "") {
            document.querySelector(".UserInformation .streetAddressInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px rgb(209, 0, 0)"
            );
        } else {
            document.querySelector(".UserInformation .streetAddressInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px grey"
            );
        }

        if (postNumber === "") {
            document.querySelector(".UserInformation .postNumberInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px rgb(209, 0, 0)"
            );
        } else {
            document.querySelector(".UserInformation .postNumberInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px grey"
            );
        }

        if (city === "") {
            document.querySelector(".UserInformation .cityInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px rgb(209, 0, 0)"
            );
        } else {
            document.querySelector(".UserInformation .cityInput")?.setAttribute(
                "style", "box-shadow: 0 0 2px grey"
            );
        }

        // If none of the input fields are empty
        if (username !== "" &&
            password !== "" &&
            streetAddress !== "" &&
            postNumber !== "" &&
            city !== "") {
                // If at least one of the input fields are modified
                if (this.state.username !== this.props.user.username ||
                    this.state.password !== "thisisnothterealpassword" ||
                    this.state.streetAddress !== this.props.user.streetAddress ||
                    this.state.postNumber !== this.props.user.postNumber ||
                    this.state.city !== this.props.user.city) {
                        const updatedUser = {
                            username: username,
                            password: password,
                            streetAddress: streetAddress,
                            postNumber: postNumber,
                            city: city
                        }

                        // Show the loading image so the user knows something is happening
                        document.querySelector(".UserInformation .profileInfoForm .LoadingImage")?.setAttribute("style", "background-color: rgb(230, 230, 230)");
                        document.querySelector(".UserInformation .profileInfoForm .LoadingImage img")?.setAttribute("style", "display: inline");
                        document.querySelector(".UserInformation .profileInfoForm .LoadingImage")?.setAttribute("style", "width: 105%");

                        // Wait for the user informations update process to finish after it's started
                        new Promise(async (resolve, reject) => {
                            const response: boolean = await this.props.saveUserInfo(updatedUser);

                            if (response) {
                                resolve("Userinformation saved into the database successfully!");
                            } else {
                                reject("Could not save userinformation for unknown reason!");
                            }
                        })
                        .then(
                            (value) => {
                                history.goBack();
                            },
                            (error) => {
                                console.log(error);
                                // Set the username input field's shadow color back to normal in case it's not
                                document.querySelector(".UserInformation .usernameInput")?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
                                // Hide the loading image
                                document.querySelector(".UserInformation .profileInfoForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
                                document.querySelector(".UserInformation .profileInfoForm .LoadingImage img")?.setAttribute("style", "display: none");
                                document.querySelector(".UserInformation .profileInfoForm .LoadingImage")?.setAttribute("style", "width: 0%");
                            }
                        );
                }
        }

        event.preventDefault();
    };

    /**
     * Send the data to the login function for authenticating the user
     * if they want to modify credentials
     * @param event: React.FormEvent
     */
    submitForm2 = (event: React.FormEvent) => {
        // Show the loading image so the user knows something is happening
        document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "background-color: rgb(170, 0, 0)");
        document.querySelector(".UserInformation .authForm .LoadingImage img")?.setAttribute("style", "display: inline");
        document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "width: 105%");

        // Call the login function and wait it to finish
        new Promise (async (resolve, reject) => {
            const response: boolean = await this.props.login(
                this.props.user.username,
                this.state.oldPassword
            );

            if (response) {
                this.setState({ isAuthenticated: true });
                resolve('Valid creadentials!');
            } else {
                reject('Invalid credentials!');
            }
        })
        .then(
            (value) => {
                // Hide the authentication form and set the oldPasswordInput's
                // shadow back to normal in case it's not
                document.querySelector(".UserInformation .authForm")?.setAttribute("style", "display: none");
                document.querySelector(".UserInformation .authForm .oldPasswordInput")?.setAttribute(
                    "style", "background-color: rgb(255, 255, 255)"
                );
                // Hide the loading image
                document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
                document.querySelector(".UserInformation .authForm .LoadingImage img")?.setAttribute("style", "display: none");
                document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "width: 0%");
                console.log(value);
            },
            (error) => {
                // Set the oldPasswordInput's shadow to normal in case it's not
                document.querySelector(".UserInformation .authForm .oldPasswordInput")?.setAttribute(
                    "style", "background-color: rgba(209, 140, 140, 0.7)"
                );
                // Hide the loading image
                document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
                document.querySelector(".UserInformation .authForm .LoadingImage img")?.setAttribute("style", "display: none");
                document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "width: 0%");
                console.log(error);
            }
        );

        event.preventDefault();
    };

    /**
     * Execute this when this component is loaded
     */
    componentDidMount() {
        // Hide the loading image
        document.querySelector(".UserInformation .profileInfoForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
        document.querySelector(".UserInformation .profileInfoForm .LoadingImage img")?.setAttribute("style", "display: none");
        document.querySelector(".UserInformation .profileInfoForm .LoadingImage")?.setAttribute("style", "width: 0%");
        document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
        document.querySelector(".UserInformation .authForm .LoadingImage img")?.setAttribute("style", "display: none");
        document.querySelector(".UserInformation .authForm .LoadingImage")?.setAttribute("style", "width: 0%");
        // Mark the current paths link color to black in the top bar
        this.props.markCurrentPage();
    }

    /**
     * Create the UserInformation component
     */
    render() {
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        return(
            <div className="UserInformation">
                {/* Title */}
                <h1>{`${myStrings.title}`}</h1>

                {/* Profile information form */}
                <form className="profileInfoForm" onSubmit={this.submitForm}>
                    <div className="loading-content">
                        {/* Input fields */}
                        <label>
                            {myStrings.title2}
                            <input
                                className="usernameInput"
                                name="username"
                                type="text"
                                value={this.state.username}
                                onChange={this.handleInputChange}
                                onClick={() => {
                                    if (!this.state.isAuthenticated) {
                                        document.querySelector(".UserInformation .authForm")?.setAttribute("style", "display: block")
                                    }
                                }} />
                        </label>

                        <label>
                            {myStrings.title3}
                            <input
                                className="passwordInput"
                                name="password"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleInputChange}
                                onClick={() => {
                                    if (!this.state.isAuthenticated) {
                                        document.querySelector(".UserInformation .authForm")?.setAttribute("style", "display: block")
                                    }
                                }}/>
                        </label>

                        <label>
                            {myStrings.title4}
                            <input
                                className="streetAddressInput"
                                name="streetAddress"
                                type="text"
                                value={this.state.streetAddress}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title5}
                            <input
                                className="postNumberInput"
                                name="postNumber"
                                type="text"
                                value={this.state.postNumber}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title6}
                            <input
                                className="cityInput"
                                name="city"
                                type="text"
                                value={this.state.city}
                                onChange={this.handleInputChange} />
                        </label>

                        <LoadingImage />
                    </div>

                    {/* Form buttons */}
                    <button className="okButton" type="submit" style={
                        (this.state.username === this.props.user.username &&
                        this.state.password === "thisisnothterealpassword" &&
                        this.state.streetAddress === this.props.user.streetAddress &&
                        this.state.postNumber === this.props.user.postNumber &&
                        this.state.city === this.props.user.city) ?
                            { filter: "brightness(85%)" } : { filter: "brightness(115%)" }
                    }>{myStrings.button}</button>
                    <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button2}</button>
                </form>

                {/* Authentication form */}
                <form className="authForm" onSubmit={this.submitForm2}>
                    <div className="authForm-wrapper">
                        <h3>{myStrings.message}</h3>

                        <div className="loading-content">
                            <label>
                                {myStrings.title7}
                                <input
                                    className="oldPasswordInput"
                                    name="oldPassword"
                                    type="password"
                                    value={this.state.oldPassword}
                                    onChange={this.handleInputChange} />
                            </label>
                            
                            <LoadingImage />
                        </div>

                        {/* Form buttons */}
                        <button className="okButton" type="submit">{myStrings.button3}</button>
                        <button className="cancelButton" type="button" onClick={() => {
                            document.querySelector(".UserInformation .authForm")?.setAttribute("style", "display: none");
                            document.querySelector(".oldPasswordInput")?.setAttribute("style", "background-color: rgb(255, 255, 255)");
                        }}>
                                {myStrings.button4}
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

export default UserInformation;