import React from 'react';
import loadIcon from '../../utils/images/load-icon.svg';
import './LoadingImage.css';

type Props = {};
type State = {};

class LoadingImage extends React.Component<Props, State> {
    render() {
        return (
            <div className="LoadingImage">
                <img src={loadIcon} alt="Loading icon" />
            </div>
        );
    }
}

export default LoadingImage;