import React from 'react';
import LangContext from '../../utils/models/LangContext';
import { Product } from '../../utils/models/types';
import history from '../../utils/history';
import strings from './Pizza.json';
import './Pizza.css';

type Props = {
    product: Product
};
type State = {};

class Pizza extends React.Component<Props, State> {

    // Get the context for setting correct
    // strings depending on the chosen language
    static contextType = LangContext;

    // Create the Pizza component
    render() {
        let color: string;
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        // Set the marker color for this pizza depending on its category
        if (this.props.product.category === "meat") {
            color = "rgb(170, 0, 0)";
        } else if (this.props.product.category === "vege") {
            color = "rgb(51, 120, 0)";
        } else {
            color = "black";
        }
            
        return (
            <button className="Pizza"
                onClick={() => history.push(`/menu/${this.props.product.id + 1}`)}>
                {/* Marker */}
                <div className="marker" style={{ backgroundColor: color }}></div>

                {/* Header */}
                <div className="Pizza-header">
                    <h3 className="Pizza-name">
                        #{this.props.product.id + 1} {this.props.product.name}
                    </h3>
                    <div className="Pizza-cost">{this.props.product.cost.toFixed(2)}€</div>
                </div>

                {/* Components */}
                <div className="Pizza-components">
                    {/* Base */}
                    <span className="title first">{myStrings.title}:</span>
                    <span>{this.props.product.components.base}</span>

                    {/* Sauce */}
                    <span className="title">{myStrings.title2}:</span>
                    <span>{this.props.product.components.sauce}</span>
                    
                    {/* Toppings */}
                    <span className="title last">{myStrings.title3}:</span>
                        {this.props.product.components.toppings.map((topping, i) =>
                            <span key={i}>{topping}</span>)}
                </div>
            </button>
        );
    }
}

export default Pizza;