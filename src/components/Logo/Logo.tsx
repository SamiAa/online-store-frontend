import React from 'react';
import logo from '../../utils/images/logo5.svg';
import './Logo.css';

class Logo extends React.Component {
    
    render() {
        return (
            <div className="Logo">
                <img src={logo} alt="logo" />
                <h1>Luigis Pizza</h1>
            </div>
        );
    }
}

export default Logo;