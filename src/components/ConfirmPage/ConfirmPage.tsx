import React from 'react';
import history from '../../utils/history';
import LangContext from '../../utils/models/LangContext';
import { ArrayOfOrders, User } from '../../utils/models/types';
import LoadingImage from '../LoadingImage/LoadingImage';
import strings from './ConfirmPage.json';
import './ConfirmPage.css';

type Props = {
    user: User,
    cart: ArrayOfOrders,
    confirmOrder: Function
};
type State = {
    streetAddress: string,
    postNumber: string,
    city: string,
    paymentMethod: string
};

class ConfirmPage extends React.Component<Props, State> {

    // Get the context for setting correct strings
    static contextType = LangContext;

    state = {
        streetAddress: "",
        postNumber: "",
        city: "",
        paymentMethod: "Cash"
    };

    /**
     * Change the correct state value when input field values are modified
     * @param event: React.ChangeEvent<HTMLInputElement>
     */
    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value } as Pick<State, keyof State>);
    };

    /**
     * Change the correct state value when the select element is modified
     * @param event: React.ChangeEvent<HTMLSelectElement>
     */
    handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({ paymentMethod: event.target.value });
    };

    /**
     * Prepare the information for sending it to the order confirmation function
     * View correct messages after the awaited asynchronous function is finished
     * @param event: React.FormEvent
     */
    submitForm = (event: React.FormEvent) => {
        const streetAddressInput = document.querySelector(".ConfirmPage .streetAddressInput");
        const postNumberInput = document.querySelector(".ConfirmPage .postNumberInput");
        const cityInput = document.querySelector(".ConfirmPage .cityInput");
        const address = {
            streetAddress: this.state.streetAddress,
            postNumber: this.state.postNumber,
            city: this.state.city
        };

        // Set input fields shadow color red if it's empty

        if (address.streetAddress === "" || address.streetAddress === undefined) {
            streetAddressInput?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
        } else {
            streetAddressInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
        }

        if (address.postNumber === "" || address.streetAddress === undefined) {
            postNumberInput?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
        } else {
            postNumberInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
        }

        if (address.city === "" || address.streetAddress === undefined) {
            cityInput?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
        } else {
            cityInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
        }

        // If there are no empty input fields
        if (address.streetAddress !== "" &&
            address.streetAddress !== undefined &&
            address.postNumber !== "" &&
            address.postNumber !== undefined &&
            address.city !== "" &&
            address.city !== undefined) {
            // Set input fields back to normal
            streetAddressInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
            postNumberInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
            cityInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
            // Hide the loading image
            document.querySelector(".ConfirmPage .LoadingImage")?.setAttribute("style", "background-color: rgb(230, 230, 230)");
            document.querySelector(".ConfirmPage .LoadingImage img")?.setAttribute("style", "display: inline");
            document.querySelector(".ConfirmPage .LoadingImage")?.setAttribute("style", "width: 105%");

            // Wait for the orders confirmation function to finish
            new Promise(async (resolve, reject) => {
                const response: boolean = await this.props.confirmOrder(address, this.state.paymentMethod);

                if (response) {
                    resolve('Order created successfully!');
                } else {
                    reject('Ordering process stopped before completion for some reason!');
                }
            })
            .then(
                (value) => {
                    history.push("/menu");
                },
                (error) => {
                    console.log(error);
                    // On error, the loading image should be cleared and message shown
                    document.querySelector(".ConfirmPage .message")?.setAttribute("style", "visibility: visible");
                    document.querySelector(".ConfirmPage .LoadingImage")?.setAttribute("style", "background-color: transparent");
                    document.querySelector(".ConfirmPage .LoadingImage img")?.setAttribute("style", "display: none");
                    document.querySelector(".ConfirmPage .LoadingImage")?.setAttribute("style", "width: 0%");
                }
            );
        }

        event.preventDefault();
    };

    /**
     * Execute this on load of this component
     */
    componentDidMount = () => {
        const address = {
            streetAddress: this.props.user.streetAddress,
            postNumber: this.props.user.postNumber,
            city: this.props.user.city
        }

        if (address.streetAddress !== undefined &&
            address.postNumber !== undefined &&
            address.city !== undefined) {
            this.setState({
                streetAddress: address.streetAddress,
                postNumber: address.postNumber,
                city: address.city
            });
        }

        // Hide the loading image
        document.querySelector(".ConfirmPage .LoadingImage")?.setAttribute("style", "background-color: transparent");
        document.querySelector(".ConfirmPage .LoadingImage img")?.setAttribute("style", "display: none");
        document.querySelector(".ConfirmPage .LoadingImage")?.setAttribute("style", "width: 0%");
    };
    
    /**
     * Create the ConfirmPage
     */
    render() {
        let myStrings = strings.EN;

        // Set correct strings depending on the selected language
        if (this.context === "FI") myStrings = strings.FI;

        return (
            <div className="ConfirmPage">
                {/* Title */}
                <h1>{myStrings.title}</h1>

                {/* Form */}
                <form className="paymentMethodForm" onSubmit={this.submitForm}>
                    <div className="loading-content">
                        <label>
                            {myStrings.title2}
                            <input
                                className="streetAddressInput"
                                name="streetAddress"
                                type="text"
                                value={this.state.streetAddress}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title3}
                            <input
                                className="postNumberInput"
                                name="postNumber"
                                type="text"
                                value={this.state.postNumber}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title4}
                            <input
                                className="cityInput"
                                name="city"
                                type="text"
                                value={this.state.city}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title5}
                            <select value={this.state.paymentMethod} onChange={this.handleSelectChange}>
                                <option value="Cash">{myStrings.title6}</option>
                                <option value="Credit card">{myStrings.title7}</option>
                            </select>
                        </label>

                        <div className="message">{myStrings.message}</div>

                        <LoadingImage />
                    </div>

                    {/* Buttons */}
                    <button className="okButton" type="submit">{myStrings.button}</button>
                    <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button2}</button>
                </form>

            </div>
        );
    }
}

export default ConfirmPage;