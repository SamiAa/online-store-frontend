import React from 'react';
import LangContext from '../../utils/models/LangContext';
import { Link } from 'react-router-dom';
import history from '../../utils/history';
import LoadingImage from '../LoadingImage/LoadingImage';
import strings from './SignUpForm.json';
import './SignUpForm.css';

type Props = {
    signup: Function
};
type State = {
    username: string,
    password: string,
    password2: string,
    streetAddress: string,
    postNumber: string,
    city: string
};

class SignUpForm extends React.Component<Props, State> {

    // Get the context for setting correct strings depending on chosen language
    static contextType = LangContext;

    state = {
        username: "",
        password: "",
        password2: "",
        streetAddress: "",
        postNumber: "",
        city: ""
    };

    /**
     * Change the value of correct state depending on the modified input field
     * @param event: React.ChangeEvent<HTMLInputElement> 
     */
    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value } as Pick<State, keyof State>);
    };

    /**
     * Prepare data for sending it to the backend for signing up
     * @param event: React.FormEvent
     */
    submitForm = (event: React.FormEvent) => {
        const username = this.state.username;
        const password = this.state.password;
        const password2 = this.state.password2;
        const streetAddress = this.state.streetAddress;
        const postNumber = this.state.postNumber;
        const city = this.state.city;
        const usernameInput = document.querySelector(".SignUpForm .usernameInput");
        const passwordInput = document.querySelector(".SignUpForm .passwordInput");
        const password2Input = document.querySelector(".SignUpForm .password2Input");
        const streetAddressInput = document.querySelector(".SignUpForm .streetAddressInput");
        const postNumberInput = document.querySelector(".SignUpForm .postNumberInput");
        const cityInput = document.querySelector(".SignUpForm .cityInput");

        // If all input fields are filled, execute this
        if (username !== "" &&
            password !== "" &&
            password2 !== "" &&
            streetAddress !== "" &&
            postNumber !== "" &&
            city !== "") {
                // Sets all input fields shadow to normal incase they weren't
                usernameInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
                passwordInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
                password2Input!.setAttribute("style", "box-shadow: 0 0 2px grey");
                streetAddressInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
                postNumberInput!.setAttribute("style", "box-shadow: 0 0 2px grey");
                cityInput!.setAttribute("style", "box-shadow: 0 0 2px grey");

                // If both password fields have the same string as a value, execute this
                if (password === password2) {
                    let isCorrect: boolean;
                    

                    // Show the loading image so the user knows something is happening
                    document.querySelector(".SignUpForm .LoadingImage")?.setAttribute("style", "background-color: rgb(230, 230, 230)");
                    document.querySelector(".SignUpForm .LoadingImage img")?.setAttribute("style", "display: inline");
                    document.querySelector(".SignUpForm .LoadingImage")?.setAttribute("style", "width: 105%");

                    // Start the sign up process and wait it to end
                    new Promise(async (resolve, reject) => {
                        isCorrect = await this.props.signup({
                            username: username,
                            password: password,
                            streetAddress: streetAddress,
                            postNumber: postNumber,
                            city: city
                        });
                        
                        if (isCorrect)
                            resolve('Signed up successfully!');
                        else
                            reject('User with the given username already exists!');
                    })
                    .then(
                        (value) => {
                            history.push("/login");
                        },
                        (error) => {
                            console.log(error);
                            // Set the username input's shadow to normal in case it's not
                            usernameInput?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
                            // Show the error message
                            document.querySelector(".SignUpForm .message3")?.setAttribute("style", "display: block");
                            // Hide the loading image
                            document.querySelector(".SignUpForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
                            document.querySelector(".SignUpForm .LoadingImage img")?.setAttribute("style", "display: none");
                            document.querySelector(".SignUpForm .LoadingImage")?.setAttribute("style", "width: 0%");
                        }
                    );
                    
                    // Hide these messages
                    document.querySelector(".SignUpForm .message")?.setAttribute("style", "display: none");
                    document.querySelector(".SignUpForm .message2")?.setAttribute("style", "visibility: hidden");
                    // Set the shadow of these input element back to normal
                    passwordInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
                    password2Input?.setAttribute("style", "box-shadow: 0 0 2px grey");
                } else { // If the password fields included different strings
                    // Show correct message
                    document.querySelector(".SignUpForm .message")?.setAttribute("style", "display: none");
                    document.querySelector(".SignUpForm .message2")?.setAttribute("style", "visibility: visible");
                    document.querySelector(".SignUpForm .message3")?.setAttribute("style", "display: none");
                    // Set these input fields shadow red
                    passwordInput?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
                    password2Input?.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
                }
        } else { // If some of the input fields were not filled, execute this
            // Show correct message
            document.querySelector(".SignUpForm .message")?.setAttribute("style", "display: block");
            document.querySelector(".SignUpForm .message2")?.setAttribute("style", "visibility: hidden");
            document.querySelector(".SignUpForm .message3")?.setAttribute("style", "display: none");

            // Set empty input fields shadow red, and others shadow to normal

            if (username === "")
                usernameInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            else
                usernameInput?.setAttribute("style", "box-shadow: 0 0 2px grey");

            if (password === "")
                passwordInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            else
                passwordInput?.setAttribute("style", "box-shadow: 0 0 2px grey");

            if (password2 === "")
                password2Input!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            else
                password2Input?.setAttribute("style", "box-shadow: 0 0 2px grey");

            if (streetAddress === "")
                streetAddressInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            else
                streetAddressInput?.setAttribute("style", "box-shadow: 0 0 2px grey");

            if (postNumber === "")
                postNumberInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            else
                postNumberInput?.setAttribute("style", "box-shadow: 0 0 2px grey");

            if (city === "")
                cityInput!.setAttribute("style", "box-shadow: 0 0 2px rgb(209, 0, 0)");
            else
                cityInput?.setAttribute("style", "box-shadow: 0 0 2px grey");
        }

        event.preventDefault();
    };

    // Execute this on load of this component
    componentDidMount() {
        document.querySelector(".SignUpForm .LoadingImage")?.setAttribute("style", "background-color: transparent");
        document.querySelector(".SignUpForm .LoadingImage img")?.setAttribute("style", "display: none");
        document.querySelector(".SignUpForm .LoadingImage")?.setAttribute("style", "width: 0%");
    }

    // Create the SignUpForm component
    render() {
        let myStrings = strings.EN;

        // Set correct strings depending on the chosen language
        if (this.context === "FI") myStrings = strings.FI;

        return (
            <div className="SignUpForm">
                {/* Title */}
                <h1>{myStrings.title}</h1>

                {/* Form */}
                <form onSubmit={this.submitForm}>
                    <div className="loading-content">
                        {/* Input fields */}
                        <label>
                            {myStrings.title2}
                            <input
                                className="usernameInput"
                                name="username"
                                type="text"
                                value={this.state.username}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title3}
                            <input
                                className="passwordInput"
                                name="password"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title4}
                            <input
                                className="password2Input"
                                name="password2"
                                type="password"
                                value={this.state.password2}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title5}
                            <input
                                className="streetAddressInput"
                                name="streetAddress"
                                type="text"
                                value={this.state.streetAddress}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title6}
                            <input
                                className="postNumberInput"
                                name="postNumber"
                                type="text"
                                value={this.state.postNumber}
                                onChange={this.handleInputChange} />
                        </label>

                        <label>
                            {myStrings.title7}
                            <input
                                className="cityInput"
                                name="city"
                                type="text"
                                value={this.state.city}
                                onChange={this.handleInputChange} />
                        </label>

                        {/* Messages */}
                        <div className="messageWrapper">
                            <div className="message">{myStrings.message}</div>
                            <div className="message2">{myStrings.message2}</div>
                            <div className="message3">{myStrings.message3}</div>
                            <div className="message4">
                                <Link to="/login">{myStrings.message4}</Link>
                            </div>
                        </div>

                        <LoadingImage />
                    </div>

                    {/* Navigation buttons */}
                    <button className="okButton" type="submit">{myStrings.button}</button>
                    <button className="cancelButton" type="button" onClick={() => history.goBack()}>{myStrings.button2}</button>
                </form>
            </div>
        );
    }
}

export default SignUpForm;